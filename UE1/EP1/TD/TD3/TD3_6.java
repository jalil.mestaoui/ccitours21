
/*

à Retenir:
			
		-->float somme = 0.0 , erreur conflit avec double
		   on ecrit float somme = 0.0f

		--> attention += et different de =+

		--> sc.nextChar() n'est pas supporté !,
			sc.next().charAt(0);


*/




import java.util.Scanner ;

public class TD3_6
{

	public static void main( String [] args)
	{

		Scanner sc = new Scanner (System.in);

		
		// Declaration des variables
		double note  ;
		double somme = 0.0;
		int nombreEtudiants=0;
		char choixContinerSaisie ;


		// La boucle pour calculer la moyenne
		do
		{

			System.out.println("saisir une note");
			note = sc.nextDouble();

			somme += note ;// somme = somme + note
			nombreEtudiants ++ ; // nombreEtudiant = nombreEtudiant + 1


			System.out.println("si vous voulez continuer la saisie des notes? Tapez O");
			System.out.println("si vous voulez afficher la moyenne des notes et quiter? Tapez N") ;

			choixContinerSaisie = sc.next().charAt(0);

		} while( choixContinerSaisie != 'N') ;

		
		// Calculer et afficher la moyenne
		System.out.println("la moyenne des notes est:"+(somme/nombreEtudiants));

	}

		
}