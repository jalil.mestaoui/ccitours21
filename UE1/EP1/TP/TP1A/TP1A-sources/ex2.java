import java.util.Arrays;

public class ex2 {

	/**
	 * Moyenne d'un tableau d'entiers 
	 * @param t tableau des éléments dont on veut faire la moyenne
	 * @return moyenne des élements du tableau 
	 */
	public static double moyenne (int[] t) {
		
		int somme = t[0];
		for (int i=1; i<=t.length; i++) {
		    somme += t[i];
		}
		
		return (somme / t.length);
	}
	
	public static void main(String [] args) {
		
		int t[] = {1, 4, 5, 8};
		double r1 = moyenne(t);
		System.out.println("moyennne des éléments : " + Arrays.toString(t) + " = " + r1);
		
	}
}
