public class ex3 {

	public static double recherche_pi () {
		
		double pi = Math.PI;
		double min = 3;
		double max = 4;
		double res;
		
		do {
			res = (min + max) / 2;
			System.out.println(res);
			if (res < pi) 
				max = res;
			else
				min = res;
		} while (res != pi);
	
		return res;
	}
	
	public static void main(String [] args) {
		
		double r1 = recherche_pi();
		System.out.println("J'ai trouvé PI : " + r1);
	}
}
