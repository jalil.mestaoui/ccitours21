


public class exo1 {

	public static void main(String[] args) {

		resoudre(-2, 2, 3);

	}

	/**
		* calcul des racines de l'équation i.x<sup>2</sup> + j.x + k = 0
		* la méthode calcule de discriminant, les solutions et les affiche
		* @param i coefficient de x<sup>2</sup>
		* @param j coefficient de x
		* @param k troisième terme de l'équation
		* @see delta
	*/

	public static void resoudre(double i, double j, double k) {

		double d = delta(i,j,k);
		if (d<0)
			System.out.println("pas de solutions");

		if (d==0.) {
			double s = -j / (2 * i);
			System.out.println("une solution : " + s);
		}

		if (d>0) {
			double s1 = ( -j - Math.sqrt(d) ) / (2 * i);
			double s2 = ( -j + Math.sqrt(d) ) / (2 * i);
			System.out.println("solution 1 : " + s1);
			System.out.println("solution 2 : " + s2);
		}
	}

	private static double delta(double i, double j, double k) {
		return ( j * j - 4 * i * k );
	}

}
