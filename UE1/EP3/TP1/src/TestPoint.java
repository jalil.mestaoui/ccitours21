

/*
         --> Constructeur de copie ou clone()
*/


public class TestPoint
{

	public void test( double expectedValue ,double returnedValue, String testedMethod )
	{

		if (!(returnedValue == expectedValue)) 

			System.out.println("expectedValue = "+expectedValue+
				             " returnedValue = "+returnedValue+"\t[Error]");
		
		else System.out.println(testedMethod+"\t[ok]");

	}


	public static void main(String [] args)
	{
		System.out.println("") ;

		Point p1 ;
		Point p2 ;

		p1 = new Point();
		(new TestPoint()).test(0. , p1.getX(),"Test origine.getX()" );
		(new TestPoint()).test(0. , p1.getY(),"Test origine.getY()" );

		System.out.println("") ;

		p1 = new Point(3.,2. );
		(new TestPoint()).test(3. , p1.getX(),"Test new(3.,2.).getX()" );
		(new TestPoint()).test(2. , p1.getY(),"Test new(3.,2.).getY()" );

		System.out.println("") ;

		p2 = new Point(p1);
		(new TestPoint()).test(3. , p2.getX(),"Test copy(3.,2.).getX()" );
		(new TestPoint()).test(2. , p2.getY(),"Test copy(3.,2).getY()" );

		System.out.println("") ;

		p1.setX(4.) ;
		p1.setY(7.) ;	
		(new TestPoint()).test(4. , p1.getX(),"Test setX(4.).getX()" );
		(new TestPoint()).test(7. , p1.getY(),"Test setY(7.).getY()" );

		System.out.println("") ;

		p1.translate(2.,3.) ;	
		(new TestPoint()).test(6. , p1.getX(),"Test (2. ,3.).translate(4.,7.)" );
		(new TestPoint()).test(10. , p1.getY(),"Test (2. ,3.).translate(4.,7.)" );

        System.out.println("") ;

	}

}
