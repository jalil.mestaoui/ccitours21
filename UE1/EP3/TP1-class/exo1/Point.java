/*
   ---> attention : pour le constructeur 

   			public Point(int abs,int ord)
			{
				x = abs ;
				y = ord ;
			}

			public void  Point(int abs,int ord)
			{
				x = abs ;
				y = ord ;
			}

			ne fonctionne pas , pas de type de 
			retour dans le constructeur !!!!!!!

	--->  utilisation de mot this 

		  public Point(int x,int y)
		  {
			x = x ;
			y = x ;
		  } // la sortie = (0,0)


		  public Point(int x,int y)
		  {
			this.x = x ;
			this.y = x ;
		  } // la sortie = les valeurs renseigné!!!

		  jdk officiel utilise la portée la plus proche, 
		  variable = variable ne va absolument pas utiliser 
		  l'attribut de la classe.

	---> this peut etre aussi utiliser pour passer l'objet instancié
		 comme parametre a une autre classe :

		 public class MaClasse
		 {
   			private int maVariable
    
   			public void fonction(int variable)
  			{
       			maVariable=variable;
      			 new classeQuelconque(this);
   			}
		 }
 
		 class classeQuelconque
		 {
  			 public classeQuelconque(maClasse classe)
   		     {
    		 Sytem.out.println(classe.toString);
  		     }
         }

    
    ---> private final int n = 2 ; // declarer une constante
    	 (peut etre declarer une seul fois sinon erreur 
    	 de compilation)


    ---> +name affiche le code asci du lettre!
    

*/



public class Point
{

	private double x;
	private double y;


	public Point()
	{
		this.x = 0 ;
		this.y = 0 ;
	}	

	public Point(Point p)
	{
		this.x = p.x ;
		this.y = p.y ;
	}			

	public Point(double x,double y )
	{
		this.x = x ;
		this.y = y ;
	}

	public double getX()
	{
		return x;
	}

	public double getY()
	{
		return y;
	}

	


	public void setX(double x)
	{
		this.x = x;
	}

	public void setY(double y)
	{
		this.y = y;

	}
	

	public void translate (double dx , double dy)
	{
		x += dx ;
		y += dy ;
	}


}